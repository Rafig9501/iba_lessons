package lesson09.warmup.t1;

import static lesson09.warmup.t1.Formatter0.print;

public class FormatterApp1 {

  public static void main(String[] args) {

    Formatter0 fmt1 = new Formatter1();
    Formatter0 fmt2 = new Formatter2();
    Formatter0 fmt3 = new Formatter3();

    // task1
    print("Hello", fmt1); // hello
    print("Hello", fmt2); // HELLO
    print("Hello", fmt3); // ***********
                                 // *  Hello  *
                                 // ***********
  }
}
