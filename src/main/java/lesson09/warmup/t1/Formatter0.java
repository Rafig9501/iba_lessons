package lesson09.warmup.t1;

public class Formatter0 {

    public static void print(String string, Formatter0 formatter0) {

        if (formatter0.getClass() == Formatter1.class) {
            System.out.println(string.toLowerCase());
        }
        else if (formatter0.getClass() == Formatter2.class){
            System.out.println(string.toUpperCase());
        }
        else if (formatter0.getClass() == Formatter3.class){
            System.out.println("**********");
            System.out.println("*  " + string + " *");
            System.out.println("**********");
        }
    }
}
