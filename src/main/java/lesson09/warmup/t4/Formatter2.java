package lesson09.warmup.t4;

public class Formatter2 {

    String string;

    public Formatter2(String string) {
        this.string = string;
    }

    public Formatter2(Formatter3 formatter3) {
        System.out.println("**********");
        System.out.println("*  " + formatter3.string.toUpperCase() + " *");
        System.out.println("**********");
    }

    @Override
    public String toString() {
        return string.toUpperCase();
    }
}
