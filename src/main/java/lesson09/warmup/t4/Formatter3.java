package lesson09.warmup.t4;

public class Formatter3 {

    String string;

    public Formatter3(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "**********" + "\n*  " + string + " *" + "\n**********" ;
    }
}
