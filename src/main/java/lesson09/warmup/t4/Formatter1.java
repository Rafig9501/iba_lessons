package lesson09.warmup.t4;

public class Formatter1 {

    String string;

    public Formatter1(String string) {
        this.string = string;
    }

    public Formatter1(Formatter3 formatter3) {
        System.out.println("**********");
        System.out.println("*  " + formatter3.string.toLowerCase() + " *");
        System.out.println("**********");
    }

    @Override
    public String toString() {
        return string.toLowerCase();
    }
}
