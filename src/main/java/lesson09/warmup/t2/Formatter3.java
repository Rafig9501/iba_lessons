package lesson09.warmup.t2;

class Formatter3 extends Formatter0 {

    @Override
    public void print(String hello) {

        System.out.println("**********");
        System.out.println("*  " + hello + " *");
        System.out.println("**********");
    }
}
