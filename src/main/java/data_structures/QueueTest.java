package data_structures;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueTest {

    public static void main(String[] args) {

        Queue<Car> integerQueue = new PriorityQueue<>(Comparator.comparingInt(o -> o.speed));

        integerQueue.add(new Car(40));
        integerQueue.add(new Car(150));
        integerQueue.add(new Car(80));

        System.out.println(integerQueue.peek());
        integerQueue.poll();
        System.out.println(integerQueue.peek());
    }
}
