package lesson7.task1;

public class Triangle extends Figure {

    Point point1;
    Point point2;
    Point point3;

    public Triangle(Point point1, Point point2, Point point3) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }

    @Override
    int calculatingArea() {

        int area = (point2.y-point1.y)*(point2.x-point1.x)/2;
        area = Math.abs(area);
        return area;
    }
}
