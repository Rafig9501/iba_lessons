package lesson7.task1;

public class Rectangle extends Figure {

    Point point1;
    Point point2;

    public Rectangle(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    @Override
    int calculatingArea() {

        int area = (point2.y-point1.y)*(point2.x-point1.x);
        area = Math.abs(area);
        return area;
    }
}
