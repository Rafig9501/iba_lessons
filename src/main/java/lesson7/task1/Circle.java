package lesson7.task1;

public class Circle extends Figure {

    Point point;
    int radius;

    public Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    @Override
    int calculatingArea() {

        int area = (int) (radius*radius*Math.PI);
        return area;
    }
}
