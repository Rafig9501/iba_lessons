package lesson7.task1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Circle circle1 = new Circle(new Point(2,5),5);
        Circle circle2 = new Circle(new Point(3,1),6);

        Rectangle rectangle1 = new Rectangle(new Point(2,4),new Point(1,9));
        Rectangle rectangle2 = new Rectangle(new Point(5,1),new Point(7,4));

        Triangle triangle1 = new Triangle(new Point(4,1),new Point(12,8),new Point(3,9));
        Triangle triangle2 = new Triangle(new Point(3,7),new Point(9,5),new Point(4,6));

        ArrayList <Figure> figures = new ArrayList<>();

        figures.add(circle1);
        figures.add(circle2);
        figures.add(rectangle1);
        figures.add(rectangle2);
        figures.add(triangle1);
        figures.add(triangle2);

        int total = 0;

        for (Figure figure : figures) {
            total = figure.calculatingArea() + total;
        }

        System.out.println(total);
    }
}
