package lesson7.task2;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList <Figure> figures = new ArrayList<>();

        for (int i = 0; i < RandomNumbers.randomNumberForFigureCount(); i++) {

            switch (RandomNumbers.randomNumberForChoosing()) {

                case 1:
                    figures.add(new Circle(new Point()));
                    break;
                case 2:
                    figures.add(new Rectangle(new Point(), new Point()));
                    break;
                case 3:
                    figures.add(new Triangle(new Point(), new Point(), new Point()));
                    break;
            }
        }

        int total = 0;

        for (Figure f:figures) {

            total = f.calculatingArea() + total;
        }
        System.out.println(total);
    }
}
