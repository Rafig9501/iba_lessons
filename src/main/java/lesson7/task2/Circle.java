package lesson7.task2;

public class Circle extends Figure {

    Point point;
    int radius;

    public Circle(Point point) {
        this.point = point;
        this.radius = RandomNumbers.randomNumberForPoints();
    }

    @Override
    int calculatingArea() {

        return (int) (radius*radius*Math.PI);
    }
}
