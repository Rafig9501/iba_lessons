package lesson7.task2;

public class RandomNumbers {

    static int MAX_FOR_OBJECT_COUNT = 20;
    static int MIN_FOR_OBJECT_COUNT = 10;
    static int MAX_FOR_POINT = 8;
    static int MIN_FOR_POINT = 3;
    static int MIN_FOR_CHOOSING = 1;
    static int MAX_FOR_CHOOSING = 3;

    static int randomNumberForFigureCount(){
        return (int)(Math.random() * MAX_FOR_OBJECT_COUNT) + MIN_FOR_OBJECT_COUNT;
    }

    static int randomNumberForPoints(){
        return (int)(Math.random() * MAX_FOR_POINT) + MIN_FOR_POINT;
    }

    static int randomNumberForChoosing(){
        return (int)(Math.random() * MAX_FOR_CHOOSING) + MIN_FOR_CHOOSING;
    }
}
