package lesson31.warmup.web;

public class Calculator {

    public String doOperation(String x, String y, String op) {
        int X = Integer.parseInt(x);
        int Y = Integer.parseInt(y);
        switch (op) {
            case "add":
                int add = X + Y;
                return String.valueOf(add);
            case "sub":
                int sub = X - Y;
                return String.valueOf(sub);
            case "mul":
                int mul = X * Y;
                return String.valueOf(mul);
            case "div":
                int div = X / Y;
                return String.valueOf(div);
            default:
                return "";
        }
    }
}
