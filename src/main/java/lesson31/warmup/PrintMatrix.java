package lesson31.warmup;

import java.util.Arrays;
import java.util.Collections;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrintMatrix {

    public static String dataOrdered(int length, int oneArrLength, int[][] arr) {

        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 0; i < length; i++) {
            joiner.add(Arrays.toString(arr[i]));
        }
        return String.valueOf(joiner);
    }

    public static void main(String[] args) {
        int[][] a =
                {
                        {1, 2, 3},
                        {5, 6, 7},
                        {9, 10, 11},
                        {13, 14, 15},
                        {17, 18, 19},
                        {21, 22, 23},
                };
        System.out.println(dataOrdered(a.length, a[0].length, a));
        // 1 2 3 7 6 5 9 10 11 15 14 13 17 18 19 23 22 21
    }
}
