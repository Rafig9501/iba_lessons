package alqorithms;

import java.util.Arrays;

public class MergeTwoArrays {

    public static void merge(int[] A, int aLength, int[] B, int bLength) {
        int i = aLength - 1;
        int j = bLength - 1;
        int k = aLength + bLength - 1;

        while (k >= 0) {
            if (j < 0 || (i >= 0 && A[i] > B[j]))
                A[k--] = A[i--];
            else
                A[k--] = B[j--];
        }
    }

    public static int[] sumOfArrays(int[] arrOne, int[] arrTwo) {

        int[] sum = new int[arrOne.length + arrTwo.length];

        for (int i = 0; i < arrOne.length; i++) {
            sum[i] = arrOne[i];
        }

        int j = 0;
        for (int i = arrOne.length; i < sum.length; i++) {
            sum[i] = arrTwo[j];
            j++;
        }
        return sum;
    }

    public static void sortedMerge(int[] a, int[] b, int[] res, int n, int m) {
        // Sorting a[] and b[]
        Arrays.sort(a);
        Arrays.sort(b);

        // Merge two sorted arrays into res[]
        int i = 0, j = 0, k = 0;
        while (i < n && j < m) {
            if (a[i] <= b[j]) {
                res[k] = a[i];
                i += 1;
            } else {
                res[k] = b[j];
                j += 1;
            }
            k += 1;
        }

        while (i < n) {  // Merging remaining
            // elements of a[] (if any)
            res[k] = a[i];
            i += 1;
            k += 1;
        }
        while (j < m) {   // Merging remaining
            // elements of b[] (if any)
            res[k] = b[j];
            j += 1;
            k += 1;
        }
    }

    public static void main(String[] args) {

        int[] arr1 = {2, 3, 4, 5, 6, 7};
        int[] arr2 = {8, 9, 10, 11};

    }
}
