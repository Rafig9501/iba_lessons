package alqorithms;

public class QuickSort {

    public static int partition(int[] array, int beg, int end) {

        int left, right, temp, local, flag;
        local = left = beg;
        right = end;
        flag = 0;
        while (flag != 1) {
            while ((array[local] <= array[right]) && (local != right))
                right--;
            if (local == right)
                flag = 1;
            else if (array[local] > array[right]) {
                temp = array[local];
                array[local] = array[right];
                array[right] = temp;
                local = right;
            }
            if (flag != 1) {
                while ((array[local] >= array[left]) && (local != left))
                    left++;
                if (local == left)
                    flag = 1;
                else if (array[local] < array[left]) {
                    temp = array[local];
                    array[local] = array[left];
                    array[left] = temp;
                    local = left;
                }
            }
        }
        return local;
    }

    static void quickSort(int[] arr, int beg, int end) {

        int loc;
        if (beg < end) {
            loc = partition(arr, beg, end);
            quickSort(arr, beg, loc - 1);
            quickSort(arr, loc + 1, end);
        }
    }

    // Driver program
    public static void main(String... args) {
        int i;
        int[] arr = {90, 79, 101, 45, 65, 54, 67, 89, 34, 23};
        quickSort(arr, 0, 9);
        System.out.println("\n The sorted array is: \n");
        for (i = 0; i < 10; i++)
            System.out.print(" " + arr[i]);
    }
}
