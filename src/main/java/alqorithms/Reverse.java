package alqorithms;

import java.util.Arrays;

public class Reverse {

    public static int reverseInteger(int num) {
        int rev = 0;
        while (num != 0) {
            rev = rev * 10 + num % 10;
            num = num / 10;
        }
        return rev;
    }

    public static String reverseWords(String string) {
        char[] sentence = string.toCharArray();
        int i = 0;
        for (int j = 0; j < sentence.length; j++) {
            if (sentence[j] == ' ') {
                reverse(sentence, i, j - 1);
                i = j + 1;
            }
        }
        reverse(sentence, i, sentence.length - 1);
        reverse(sentence, 0, sentence.length - 1);
        return Arrays.toString(sentence);
    }

    public static void reverse(char[] words, int i, int j) {
        while (i < j) {
            char temp = words[i];
            words[i] = words[j];
            words[j] = temp;
            i++;
            j--;
        }
    }

    public static void main(String[] args) {

        System.out.println(reverseWords("how are you"));
    }
}
