package alqorithms;

import java.util.Arrays;

public class Rotate {

    public static int[] rotate(int[] arr, int k) {

        int[] result = new int[arr.length];

        for (int i = 0; i < k; i++) {
            result[i] = arr[arr.length-k+i];
        }

        int j=0;
        while (k<arr.length){
            result[k] = arr[j];
            j++;
            k++;
        }
        return result;
    }

    public static void main(String[] args) {

        int[] arr = {4, 3, 5, 2};
        System.out.println(Arrays.toString(rotate(arr,2)));
    }
}
