package lesson24a6.warmup;

public class IsPalindrome {

    boolean check(int n) {

        String palindrome = String.valueOf(n);
        boolean is = true;

            for (int i = 0; i < palindrome.length() / 2; i++) {
                is = is && palindrome.charAt(i) == palindrome.charAt(palindrome.length() - i - 1);
            }
        return is;
    }
}
