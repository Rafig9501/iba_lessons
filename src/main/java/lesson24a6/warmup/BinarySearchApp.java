package lesson24a6.warmup;

import java.util.Arrays;
import java.util.Random;

public class BinarySearchApp {

    static int[] data = new Random().ints(0, 10000).limit(1000).toArray();
    static int[] sorted = data.clone();

    // count number of `if` statements
    // executed to find the given element
    // we need to return `boolean` and `index` and `count`
    static String find_conventional(int[] origin, int number) {
        int counter = 0;
        int index = 0;
        boolean exist = false;
        for (int i = 0; i < origin.length; i++) {
            counter++;
            if (origin[i] == number) {
                exist = true;
                index = i;
                break;
            }
        }
        String result = "index is " + index + " counter is " + counter + " exist " + exist;
        return result;
    }

    // count number of `if` statements
    // executed to find the given element
    // we need to return `boolean` and `index` and `count`
    static String find_binary(int[] origin, int number) {
        int counter = 0;
        int index = 0;
        boolean exist = false;
        int l = 0;
        int r = origin.length - 1;
        while (l < r) {
            counter++;
            int m = (l + r) / 2;
            if (number == origin[m]) {
                index = m;
                exist = true;
                break;
            }
            if (number > origin[m]) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        String result = "index is " + index + " counter is " + counter + " exist " + exist;
        return result;
    }


    public static void main(String[] args) {
        Arrays.sort(sorted);
//    System.out.println(Arrays.toString(data));
//    System.out.println(Arrays.toString(sorted));
//    int rnd_idx = 249+500; // it means index == m on particular iteration
        int rnd_idx = (int) (Math.random() * 1000);
        int rnd = sorted[rnd_idx];
        String result1 = find_conventional(data, rnd);
        String result2 = find_binary(sorted, rnd);
        System.out.println(result1);
        System.out.println(result2);
    }
}
