package lesson34;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class Result {
    /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
     */
    public static int diagonalDifference(List<List<Integer>> arr) {
            // Initialize sums of diagonals
            int diag1 = 0;
            int diag2 = 0;

            for (int i = 0; i < arr.size(); i++)
            {
                diag1 += arr.get(i).get(i);
                diag2 += arr.get(i).get(arr.size()-i-1);
            }
            // Absolute difference of the sums
            // across the diagonals
        return Math.abs(diag1 - diag2);
    }
}
