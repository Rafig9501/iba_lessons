package lesson35.jdbc;

import com.sun.xml.internal.stream.events.NamedEvent;

import java.sql.*;

public class SqlApp {

    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USER_NAME = "postgres";
    private final static String PASSWORD = "2101";

    public static void main(String[] args) throws SQLException {

        Connection conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        String SQL = "SELECT * FROM car";
        PreparedStatement stmt = conn.prepareStatement(SQL);
        ResultSet rset = stmt.executeQuery();
        while (rset.next()){
            int id = rset.getInt("id");
            String model = rset.getString("model");
            Date year = rset.getDate("year");
            String country = rset.getString("country");
            System.out.println(id + " " + model + " " + year + " " + country);
        }
    }
}
