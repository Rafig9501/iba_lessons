package lesson8.task1;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Task1 {

    static List<Integer> random;

    public static void main(String[] args) {

        System.out.println(showingElements());
        System.out.println(sortingUnigues());
    }

    static List<Integer> sortingUnigues() {

        return random.stream().distinct().sorted((o1, o2) -> o2-o1).collect(Collectors.toList());
    }

    static List<Integer> showingElements(){

        random = new Random().ints(30, -10, 10).boxed().collect(Collectors.toList());

        return random;
    }
}