package lesson22;

import java.util.Random;

public class TheSum {

    static int sum = 0;
    static int index = 0;

    static int sum(int[] data) {

        if (data.length <= index) {
            return sum;
        } else {
            sum = sum + data[index++];
            sum(data);
            return sum;
        }
    }

    public static void main(String[] args) {
        int[] d = new Random().ints(1, 10)
                .limit(10).toArray();
        int sum = sum(d);
        System.out.println(sum);
    }
}
