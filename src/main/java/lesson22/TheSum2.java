package lesson22;

public class TheSum2 {


    public static int sumTo(int n) {
        if (n <= 1)
            return n;
        return n + sumTo(n - 1);
    }

    public static int multipleTo(int n) {
        if (n <= 1)
            return n;
        return n * multipleTo(n - 1);
    }

    public static int fibonacci(int n) {
        if (n == 0 || n == 1) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static void main(String[] args) {

        System.out.println(fibonacci(40));
    }
}
