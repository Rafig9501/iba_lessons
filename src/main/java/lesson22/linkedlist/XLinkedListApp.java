package lesson22.linkedlist;

public class XLinkedListApp {
  public static void main(String[] args) {
    XLinkedList xl = new XLinkedList();
    xl.prepend(1); // 1
    xl.prepend(2); // 2, 1
    xl.prepend(3); // 3, 2, 1
    xl.append(10); // 3, 2, 1, 10
    xl.append(20); // 3, 2, 1, 10, 20
    System.out.println(xl.represent());
    xl.reverse();
    System.out.println(xl.represent2()); // 20, 10, 1, 2, 3
    xl.reverser();
    System.out.println(xl.represent3r());
    System.out.println(xl.length_iter());
    System.out.println(xl.length_r());
    System.out.println(xl.length_tr());
    System.out.println(xl.contains(3)); // true
    System.out.println(xl.contains(4)); // false
    xl.insertAfter(3,77); // 3, 2, 1, 77, 10, 20
    System.out.println(xl.represent());
    xl.insertBefore(3,88); // 3, 2, 88, 1, 77, 10, 20
    System.out.println(xl.represent());
    xl.insertBeforeValue2(88,87); // 3, 2, 87, 88, 1, 77, 10, 20
    System.out.println(xl.represent());
  }
}
