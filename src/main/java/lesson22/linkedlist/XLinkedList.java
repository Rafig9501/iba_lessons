package lesson22.linkedlist;

import java.util.StringJoiner;

public class XLinkedList {

    static class Node {
        final int value;
        Node next;

        Node(int value) {
            this.value = value;
        }

        Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

    }

    Node head = null;

    void prepend(int element) {
        Node node = new Node(element);
        node.next = head;
        head = node;
    }

    void prepend2(int element) {
        head = new Node(element).next = head;
    }

    void prepend3(int element) {
        head = new Node(element, head);
    }

    void append(int element) {
        Node node = new Node(element);
        if (head == null) {
            head = node;
        } else {
            Node curr = head;
            while (curr.next != null) {
                curr = curr.next;
            }
            curr.next = node;
        }
    }

    boolean containsFrom(Node curr, int element) {
        if (curr == null) return false;
        if (curr.value == element) return true;
        return containsFrom(curr.next, element);
    }

    boolean contains(int element) {
        return containsFrom(head, element);
    }

    boolean contains_iter(int element) {
        Node curr = head;
        while (curr != null) {
            if (curr.value == element) return true;
            curr = curr.next;
        }
        return false;
    }

    void insertBefore(int index, int value) {
        insertAfter(index - 1, value);
    }

    void insertBeforeValue(int valueLookFor, int value) {
        Node newNode = new Node(value);
        Node curr = head;
        Node prev = null;
        while (curr.value != valueLookFor) {
            prev = curr;
            curr = curr.next;
        }
        newNode.next = curr;
        prev.next = newNode;
    }

    void insertBeforeValue2(int valueLookFor, int value) {
        Node newNode = new Node(value);
        Node curr = head;
        while (curr.next.value != valueLookFor) {
            curr = curr.next;
        }
        newNode.next = curr.next;
        curr.next = newNode;
    }

    // we calculate from 1
    void insertAfter(int index, int value) {
        Node newNode = new Node(value);
        Node curr = head;
        while (index > 1) {
            curr = curr.next;
            index--;
        }
        newNode.next = curr.next;
        curr.next = newNode;
    }

    void insertAfter_for(int index, int value) {
        Node newNode = new Node(value);
        Node curr = head;
        for (; index > 1; curr = curr.next, index--) {
        }

        newNode.next = curr.next;
        curr.next = newNode;
    }

    public void reverse() {
        for (Node curr = head; curr != null; curr = curr.next) {

        }
    }

    public void reverser() {

    }

    void deleteHead() {
        throw new IllegalArgumentException("deleteHead:hasn't implemented yet");
    }

    void deleteLast() {
        throw new IllegalArgumentException("deleteLast:hasn't implemented yet");
    }

    void deleteAt(int index) {
        throw new IllegalArgumentException("deleteAt:hasn't implemented yet");
    }

    String represent() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        Node curr = head;
        while (curr != null) {
            sj.add(String.valueOf(curr.value));
            curr = curr.next;
        }
        return sj.toString();
    }

    String represent2() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        for (Node curr = head; curr != null; curr = curr.next) {
            sj.add(String.valueOf(curr.value));
        }
        return sj.toString();
    }

    /**
     * TO IMPLEMENT RECURSIVE IMPLEMENTATION FOR
     * LIST REPRESENTATION
     */
    private void attach_next(Node curr, StringJoiner sj) {
        if (curr == null) return;
        sj.add(String.valueOf(curr.value));
        attach_next(curr.next, sj);
    }

    public String represent3r() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        attach_next(head, sj);
        return sj.toString();
    }

    public int length_iter() {
        int len = 0;
        for (Node curr = head; curr != null; curr = curr.next) len++;
        return len;
    }

    // head-recursion
    public int length_r_from(Node curr) {
        if (curr == null) return 0;
        int partialLen = length_r_from(curr.next);
        return partialLen + 1;
    }

    // tail-recursion
    public int length_tr(Node curr, int acc) {
        if (curr == null) return acc;
        return length_tr(curr.next, acc + 1);
    }

    public int length_r() {
        return length_r_from(head);
    }

    public int length_tr() {
        return length_tr(head, 0);
    }

}
