package lesson22.warmup;

import java.util.LinkedList;
import java.util.List;

public class GCDApp {

    int gcd(int a, int b) {

        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    public static void main(String[] args) {

    }
}
