package lesson13.warmup;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StringCheckRotated {

    public boolean check(String origin, String rotated) {

        if (origin == null || rotated == null) {
            return false;
        } else if (origin.length() != rotated.length()) {
            return false;
        }
        int index = rotated.indexOf(origin.charAt(0));
        if (index > -1) {
            if (origin.equalsIgnoreCase(rotated)) {
                return true;
            }
            int finalPos = rotated.length() - index;
            return rotated.charAt(0) == origin.charAt(finalPos) && origin.substring(finalPos).equals(rotated.substring(0, index));
        }
        return false;
    }
}

//    throw new IllegalArgumentException("should be implemented");
// if (origin.length() != rotated.length()) {
//            return false;
//        }
//        String concaticated = origin + origin;
//        return concaticated.contains(rotated);
