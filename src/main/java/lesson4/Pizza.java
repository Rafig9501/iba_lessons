package lesson4;

public class Pizza {

    String name;

    int size;

    int price;

    static int count;

    public Pizza(String name, int size, int price) {
        this.name = name;
        this.size = size;
        this.price = price;

        count++;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", price=" + price +
                '}';
    }


}
