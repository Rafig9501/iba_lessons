package lesson4;

import java.util.ArrayList;

public class PizzaApp {

    public static void main(String[] args) {

        ArrayList <Pizza> pizzas = new ArrayList();

        for (int i = 0; i<10;i++){

            Pizza pizza1 = new Pizza(randomPizzaName(),randomSize(),randomPrice());

            pizzas.add(pizza1);
        }

        System.out.println(pizzas);
    }

    public static String randomPizzaName () {
        StringBuilder pizza = new StringBuilder();
        for (char i = 'A'; i <= 'H'; i++) {
            pizza.append(i);
        }
        return pizza.toString();
    }

    public static int randomPrice(){

        int MAX = 20;

        int MIN = 10;

        int price = (int) (Math.random()*(MAX - MIN + 1) + MIN);

        return price;
    }

    public static int randomSize(){

        int MAX = 30;

        int MIN = 15;

        int price = (int) (Math.random()*(MAX - MIN + 1) + MIN);

        return price;
    }
}
