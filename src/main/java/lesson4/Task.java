package lesson4;

public class Task {

    public static boolean isCapital(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public static boolean isSmall(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean isLetter(char c) {
        return isCapital(c) || isSmall(c);
    }

    public static boolean isVowel(char c) {
        final String vowels = "aeoiu";
        return vowels.indexOf(Character.toLowerCase(c)) >= 0;
    }

    public static boolean isConsonant(char c) {
        return !isVowel(c);
    }

    public static String alphabetSmall() {
        StringBuilder alpha = new StringBuilder();
        for (char i = 'a'; i <= 'z'; i++) {
            alpha.append(i);
        }
        return alpha.toString();
    }

    public static String alphabetCapital() {
        return alphabetSmall().toUpperCase();
    }

    public static char randomSmallLetter() {
        String alpha = alphabetSmall();
        return alpha.charAt((int) (Math.random()*alpha.length()));
    }

    public static char randomCapitalLetter() {
        String alpha = alphabetCapital();
        return alpha.charAt((int) (Math.random()*alpha.length()));
    }

    public static char randomLetter() {
        return ((int)(Math.random()*2)==0) ? // 0 or 1
                randomSmallLetter() : randomCapitalLetter();
    }

    public static StringBuilder smallCapitalLetter(int length){

        StringBuilder smallCapital = new StringBuilder();

        for (int i = 0;i<length;i++){

            smallCapital.append(randomLetter());
        }

        return smallCapital;
    }

    public static void main(String[] args) {
        System.out.println(smallCapitalLetter(30));
    }
}

