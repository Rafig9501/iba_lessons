package lesson4;

public class TabWithNumbers {

    public static void main(String[] args) {

        int[] row_arr = new int[8];
        int[] column_arr = new int[8];

        for (int row = 0; row < row_arr.length; row++) {

            for (int column = 0; column < column_arr.length; column++) {

                if (row==0 || column == 0  || column == column_arr.length-1){

                    System.out.print(1);
                }

                if (row-column == 1){

                    System.out.print(row);
                }
            }
            System.out.println();
        }
    }
}
