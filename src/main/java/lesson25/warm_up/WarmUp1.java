package lesson25.warm_up;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WarmUp1 {

    public static void main(String[] args) {

        List<Integer> data = new Random()
                .ints(10, 25)
                .boxed().limit(20).collect(Collectors.toList());

        data.forEach(integer -> System.out.print(" " + integer));
        System.out.println();

        HashMap<Integer, Integer> rep = new HashMap<>();
        data.forEach(x -> rep.merge(x, 1, new BiFunction<Integer, Integer, Integer>() {
            @Override
            public Integer apply(Integer a, Integer b) {
                return Integer.sum(a, b);
            }
        }));

        for (Map.Entry<Integer, Integer> entry : rep.entrySet()) {
            Integer k = entry.getKey();
            Integer v = entry.getValue();
            System.out.println("Key: " + k + ", Value: " + v);
        }

        rep.keySet().stream().map(i -> i + 2).collect(Collectors.toList())
                .forEach(integer -> System.out.print(" " + integer));
    }
}
