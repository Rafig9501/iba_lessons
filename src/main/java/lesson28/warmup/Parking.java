package lesson28.warmup;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

public class Parking {
    public static long carParkingRoof(List<Long> cars, int k) {

        Map<Long, Long> collect = cars.stream().sorted().collect(Collectors.toMap(
                new Function<Long, Long>() {
                    @Override
                    public Long apply(Long key) {
                        return key;
                    }
                }, new Function<Long, Long>() {
                    @Override
                    public Long apply(Long val) {
                        return cars.size()-val+1;
                    }
                }));
        System.out.println(collect);
        return 2;
    }

    public static void main(String[] args) {
        List<Long> cars = Arrays.asList(6L, 2L, 12L, 7L);
        long r = carParkingRoof(cars, 3);
        System.out.println(r); // 6
    }
}
