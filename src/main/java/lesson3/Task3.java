package lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task3 {

    static double tan;

    private static boolean checking (int y, int x, int height, int width){

        return  y == 1 || y == height||x == 1 || x == width || x ==(int)(y*tan) || width - x == (int) (y*tan);
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int width = Integer.parseInt(reader.readLine());

        int height = Integer.parseInt(reader.readLine());

        StringBuilder envelope = new StringBuilder("");

        tan = ((double) width)/height;

        for (int y = 1; y <= height; y++)
        {
            for (int x = 1; x <= width; x++)
            {
                if (checking(y,x,height,width))
                {
                    envelope.append("*");
                }
                else {
                    envelope.append(" ");
                }
            }
            envelope.append("\n");
        }
        System.out.println(envelope);
    }
}
