package lesson10;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

public class CharacterApp2 {
  public static void main(String[] args) {
    CharacterPositions08 app = new CharacterPositions08();
    HashMap<Character, List<Integer>> result = app.countUnique("Hello Worldd");

    List<Tuple<Character, List<Integer>>> converted = app.convert(result);

    Consumer<Tuple<Character, List<Integer>>> print = t -> System.out.printf("Char: %s, cnt:%d, list:%s\n", t.a, t.b.size(), t.b);
    Comparator<Tuple<Character, List<Integer>>> c1 = (o1, o2) -> o1.a - o2.a;
    Comparator<Tuple<Character, List<Integer>>> c2 = (o1, o2) -> o2.b.size() - o1.b.size();
    Comparator<Tuple<Character, List<Integer>>> c3 = (o1, o2) -> o1.b.get(0) - o2.b.get(0);

    Collections.sort(converted, c1);
    converted.forEach(print);
    System.out.println("------");

    Collections.sort(converted, c2);
    converted.forEach(print);
    System.out.println("------");

    Collections.sort(converted, c3);
    converted.forEach(print);
    System.out.println("------");
  }
}
