package lesson10;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CharacterPositions08 {

  public List<Tuple<Character, List<Integer>>> convert (Map<Character, List<Integer>> map) {
    Stream<Map.Entry<Character, List<Integer>>> stream = map.entrySet().stream();
    Stream<Tuple<Character, List<Integer>>> stream2 = stream.map(e -> new Tuple<>(e.getKey(), e.getValue()));
    List<Tuple<Character, List<Integer>>> collected = stream2.collect(Collectors.toList());
    return collected;
  }

  public HashMap<Character, List<Integer>> countUnique(String origin) {
    HashMap<Character, List<Integer>> data = new HashMap<>();
    for (int pos = 0; pos < origin.length(); pos++) {
      char ch = origin.charAt(pos);
      data.merge(ch, Arrays.asList(pos), (list1, list2) -> {
        ArrayList<Integer> list3 = new ArrayList<>();
        list3.addAll(list1);
        list3.addAll(list2);
        return list3;
      });
//      List<Integer> positions = data.getOrDefault(ch, new ArrayList<>());
//      positions.add(pos);
//      data.put(ch, positions);
    }
    return data;
  }
}
