package lesson10;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestMain {

    public static void main(String[] args) {

        Map<Character, Integer> map = new HashMap();

        map.put('a',1);
        map.put('b',2);
        map.put('c',3);
        map.put('d',4);
        map.put('f',5);
        map.put('e',6);

        Map<Integer, Character> swapped = map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        swapped.forEach((integer, character) -> System.out.print(character));

        map.forEach((character, integer) -> System.out.print(integer));

        System.out.println();

        map.merge('f', 4, Integer::sum);

        map.forEach((character, integer) -> System.out.print(integer));

        System.out.println();

        System.out.print(map.get('f'));
    }
}
