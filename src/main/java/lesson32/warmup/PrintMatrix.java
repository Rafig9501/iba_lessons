package lesson32.warmup;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrintMatrix {

    public static String dataOrdered(int R, int C, int[][] m) {
        return IntStream.range(0, R * C).map(idx -> {
            int col = idx / R;
            int shift = idx - col * R;
            int row = (col & 1) == 0 ? shift : R - 1 - shift;
            return m[row][col];
        })
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" "));
    }

    public static void main(String[] args) {

        int[][] arr =
                {
                        {1, 2, 3},
                        {5, 6, 7},
                        {9, 10, 11},
                        {13, 14, 15},
                        {17, 18, 19},
                        {21, 22, 23},
                };

        int f = 4;
        int e = 1;

        System.out.println(f&e);
    }
}
