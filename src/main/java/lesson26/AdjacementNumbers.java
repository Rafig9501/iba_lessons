package lesson26;

import javafx.util.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class AdjacementNumbers {

    static int index = 0;

    static int smallest_pair(int[] ints) {
        int min = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        for (int i : ints) {
            if (i < min) {
                secondMin = min;
                min = i;
            } else if ((i < secondMin) && i != min) {
                secondMin = i;
            }
        }
        return (secondMin + min);
    }

    public static void main(String[] args) {
        int[] ints = {2,3,5,1,9,6};

        String hello = "Hello World";

        Arrays.binarySearch(ints,4);
    }
}



