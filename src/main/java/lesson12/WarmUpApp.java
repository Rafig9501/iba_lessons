package lesson12;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WarmUpApp {

    public static void main(String[] args) throws IOException {

        Optional<Integer> value1 = strToInt("123");
        Optional<Integer> value2 = strToInt("123q");

        System.out.println(value1);
        System.out.println(value2);
    }

    private static Optional<Integer> strToInt(String str) {

        Optional<Integer> value = Optional.empty();

        try {
            value = Optional.of(Integer.parseInt(str));
        }
        catch (NumberFormatException e){
            return value;
        }
        return value;
    }
}