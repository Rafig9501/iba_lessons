package lesson36.warmup;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;


public class GradingApp {

    static class Result {

        public static List<Integer> gradingStudents(List<Integer> grades) {
            return grades.stream().map(i -> {
                if (i>=38 && (((5 * (Math.round(i/5)+1))-i)<3))
                    return 5 * (Math.round(i/5)+1);
                else return i;
            }).collect(Collectors.toList());
        }
    }
        public static void main(String[] args) throws IOException {

            ArrayList numbers = new ArrayList();
            numbers.add(34);
            numbers.add(67);
            numbers.add(78);
            numbers.add(38);

            List list = Result.gradingStudents(numbers);
            System.out.println(list);
        }
    }
