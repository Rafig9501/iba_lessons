package lesson36.jdbc;

import java.sql.*;

public class SqlApp {

    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USER_NAME = "postgres";
    private final static String PASSWORD = "2101";

    public static void main(String[] args) throws SQLException {

        Connection conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        String SQL = "SELECT * FROM car WHERE model = ? AND country = ?";
        PreparedStatement statement = conn.prepareStatement(SQL);
        statement.setString(1, "760");
        statement.setString(2, "China");
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            System.out.println(resultSet.getString("uuid") + " " + resultSet.getString("year"));
        }
        conn.close();
    }
}