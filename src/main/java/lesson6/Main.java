package lesson6;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Animal> animals = new ArrayList<>();

        Cat cat1 = new Cat("Mur-Mur");
        Cat cat2 = new Cat("Pur-Pur");
        Cat cat3 = new Cat("Nur-Nur");

        Dog dog1 = new Dog("Rex");
        Dog dog2 = new Dog("Tuzik");
        Dog dog3 = new Dog("Alabay");

        Fish fish1 = new Fish("Dori");
        Fish fish2 = new Fish("Lari");
        Fish fish3 = new Fish("Hari");

        Animal dragon = new Animal("Dragon"){
            @Override
            public String toString() {
                return "Hello, I am dragon, my name is " + name ;
            }
        };

        animals.add(cat1);
        animals.add(cat2);
        animals.add(cat3);
        animals.add(dog1);
        animals.add(dog2);
        animals.add(dog3);
        animals.add(fish1);
        animals.add(fish2);
        animals.add(fish3);
        animals.add(dragon);
    }
}
