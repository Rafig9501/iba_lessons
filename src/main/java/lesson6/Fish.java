package lesson6;

public class Fish extends Animal {

    public Fish(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hello, I am animal, my name is " + name;
    }
}
