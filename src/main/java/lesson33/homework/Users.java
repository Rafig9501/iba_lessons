package lesson33.homework;

import java.util.HashMap;
import java.util.Map;

public class Users {

    public static Map<String, String> USERS = new HashMap<>();

    boolean addUser(String login, String password) {
        if (USERS.containsKey(login)) {
            USERS.put(login, password);
            return true;
        }
        return false;
    }
}
