package lesson33.homework;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class LoginApp {

    public static void main(String[] args) throws Exception {

        Server server = new Server(8002);
        ServletContextHandler handler = new ServletContextHandler();

        Users users = new Users();

        handler.addServlet(new ServletHolder(new LoginServletGet()),"/logged_in");
        handler.addServlet(new ServletHolder(new LoginServletPost(users)),"/logged_in");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
