package lesson33.homework;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServletPost extends HttpServlet {

    private Users user;

    public LoginServletPost(Users user) {
        this.user = user;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter writer = resp.getWriter();
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        boolean exist = user.addUser(login, password);

        String message = exist ? "this login has already registered" : "registered succesfully";

        writer.write(message);
    }
}
