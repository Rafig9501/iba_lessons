package lesson33;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrintMatrix {

    public static String dataOrdered(int R, int C, int[][] m) {
        return IntStream.range(0, R * C).map(idx -> {
            int row = idx / C;
            int shift = idx - row * C;
            int col = (row & 1) == 0 ? shift : C - 1 - shift;
            return m[row][col];
        })
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" "));
    }

    public static void main(String[] args) {

        int[][] a =
                {
                        {1, 2, 3},
                        {5, 6, 7},
                        {9, 10, 11},
                        {13, 14, 15},
                        {17, 18, 19},
                        {21, 22, 23},
                };
    }
}
