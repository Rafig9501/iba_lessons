package lesson5;

import java.util.Arrays;

public class Test {
    static int[] array;
    static int[] posArray;
    static int[] negArray;

    public static void main(String[] args) {

        System.out.println(Arrays.toString(creatingArray()));

        shiftingArray();

    }

    static int[] creatingArray() {

        int MIN = -100;
        int MAX = 100;
        int randomNumber;

        array = new int[20];

        for (int i = 0; i < array.length; i++) {

            randomNumber = (int) (Math.random() * (MAX - MIN)) + MIN;

            array[i] = randomNumber;
        }

        return array;
    }

    static void shiftingArray() {

        for (int i = 0; i < array.length; i++) {

            if (array[i] > 0) {

                for (int j = i + 1; j < array.length; j++) {

                    if (array[j] > 0) {
                        int swapper = array[i];
                        array[i] = array[j];
                        array[j] = swapper;
                    }

                    if (array[j] < 0) {
                        int swapper = array[i];
                        array[i] = array[j];
                        array[j] = swapper;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}