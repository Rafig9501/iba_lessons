package lesson5;

public class OddEvenApp {

    static int [] oddArray;
    static int [] evenArray;

    public static void main(String[] args) {

        oddArray = new int[15];
        evenArray = new int[15];

        declaringArrays();

        combiningArrays(oddArray,evenArray);

    }

    static void declaringArrays(){

        for (int i = 0;i<oddArray.length;i++){

            int randomElement = (int) (Math.random()*100);
            randomElement = randomElement*2;

            oddArray[i] = randomElement;
        }

        for (int i = 0;i<evenArray.length;i++){

            int randomElement = (int) (Math.random()*100);
            randomElement = randomElement*2+1;

            evenArray[i]=randomElement;
        }
    }

    static int[] combiningArrays(int[]oddArray,int[]evenArray){

        int [] combinedArray = new int[oddArray.length+evenArray.length];

        for (int i = 0; i < oddArray.length; i++){

            combinedArray[i*2+1]=oddArray[i];
        }

        for (int i = 0; i < evenArray.length; i++){

            combinedArray[i*2]=evenArray[i];
        }

        for (int value : combinedArray) {

            System.out.println(value);
        }

        return combinedArray;
    }
}
