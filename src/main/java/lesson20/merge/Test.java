package lesson20.merge;

import java.util.Arrays;

public class Test {

    public static void main(String[] args) {

        int[] arr = {2,3,6,1,4,9,5,8,6,7};

        int[] leftArr = new int[4];
        int[] rightArr = new int[6];

        for (int i = 0; i < leftArr.length; i++) {
            leftArr[i] = arr[i];
        }

        for (int i = 0; i < rightArr.length; i++) {
            rightArr[i]=arr[i+4];
        }

        System.out.println(Arrays.toString(leftArr));
        System.out.println(Arrays.toString(rightArr));
    }
}
