package lesson20.sort;

public class Test {

    static void bubbleSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i]<arr[j]){
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {

        int[] arr ={3,60,35,2,45,320,5};

        System.out.println("Array Before Bubble Sort");

        for(int i=0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }

        System.out.println();

        bubbleSort(arr);//sorting array elements using bubble sort

        System.out.println("Array After Bubble Sort");

        for(int i=0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
    }
}
