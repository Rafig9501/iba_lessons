package lesson11.iterator.reverse;

public class HiddenApp {
  public static void main(String[] args) {
    Hidden data = new Hidden();
    for (String one: data) {
      System.out.println(one);
    }
  }
}
