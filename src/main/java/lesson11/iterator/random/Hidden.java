package lesson11.iterator.random;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class Hidden implements Iterable<String> {

  private final String[] months = {
      "Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  @Override
  public Iterator<String> iterator() {

    return new Iterator<String>() {

      int index = 0;
      @Override
      public boolean hasNext() {
        return index < months.length;
      }

      @Override
      public String next() {
        return months[index++];
      }
    };
  }
}
