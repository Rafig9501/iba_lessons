package lesson11.iterator.random;

import java.util.Random;

public class HiddenApp {
  public static void main(String[] args) {

    Hidden data = new Hidden();

    for (String one: data) {
      System.out.print(one);
    }
  }
}
