package lesson11.iterator;

import java.util.Iterator;

public class Hidden implements Iterable<String> {
  private final String[] months = {
      "Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  @Override
  public Iterator<String> iterator() {
    return new Iterator<String>() {
      int index = 0;
      @Override
      public boolean hasNext() {
        return index < months.length;
      }

      @Override
      public String next() {
        return months[index++];
      }
    };
  }
}
