package lesson11;

import java.util.List;
import java.util.Random;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task2 {

    public static void main(String[] args) {

        List<Integer> ints = new Random().ints(20,-10, 10)
                .boxed().collect(Collectors.toList());

        List<Integer> negative = ints.stream()
                .filter(value -> value < 0)
                .collect(Collectors.toList());

        List<Double> positive = ints.stream()
                .filter(value -> value > 0)
                .map(Math::sqrt)
                .collect(Collectors.toList());

        positive.forEach(System.out::print);
        negative.forEach(System.out::print);
    }
}
