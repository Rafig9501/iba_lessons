package lesson11;

import java.util.ArrayList;
import java.util.List;

public class MainApp {

    public static void main(String[] args) {

        List<String> subjects = new ArrayList();
        List<String> verbs = new ArrayList<>();
        List<String> objects = new ArrayList<>();

        subjects.add("Noel");
        subjects.add("The cat");
        subjects.add("The dog");

        verbs.add("wrote");
        verbs.add("chased");
        verbs.add("slept on");

        objects.add("the book");
        objects.add("the ball");
        objects.add("the bed");

        CombinationClass combinationClass = new CombinationClass();

        System.out.println(combinationClass.combinationLists(subjects,verbs,objects));
    }
}
