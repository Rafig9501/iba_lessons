package lesson30.warmup;

public class StringShift {

    public static String shift(String orig, int toLeft, int toRight) {
        orig = String.format("%s%s", orig.substring(toLeft), orig.substring(0, toLeft));
        orig = String.format("%s%s", orig.substring(orig.length() - toRight), orig.substring(0, orig.length() - toRight));
        return orig;
    }

    public static void main(String[] args) {

        String name = "abcd";
        System.out.println(name);
        System.out.println(shift(name, 1, 3));
    }
}
