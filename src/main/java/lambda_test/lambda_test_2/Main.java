package lambda_test.lambda_test_2;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class Main {

    private static List<String> listOfBooks = Arrays.asList("Effective Java", "Clean Code", "Test Driven", "How are you");

    public static String getBook(String letter) {

        Optional<String> book = listOfBooks.stream()
                .filter(b -> b.startsWith(letter))
                .findFirst();

        return book.orElse("Book Not Found");
    }

    public static void main(String[] args) {

//        System.out.println(getBook("Effective Java"));
//
//        Optional<String> optional = listOfBooks.stream().filter(s -> s.contains("a")).findAny();
//
//        System.out.println(optional);
//
//        Optional<Main> optionalMain = Optional.empty();

        List<String> payCompanies = Arrays.asList("Apple pay", "Samsung Pay", "Paypal");
        String wallats = String.join(",", payCompanies);
        System.out.println("electronic wallats : " + wallats);
    }
}
